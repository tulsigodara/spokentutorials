/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drag;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.Iterator;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JToolBar.Separator;
import spokentutorials.Slide;

/**
 *
 * @author Joey
 */
public class TimelineDropTargetListener extends DropTargetAdapter {

    private DropTarget dropTarget;
    private JPanel p;
private String name;
    public TimelineDropTargetListener(JPanel panel) {
        p = panel;
        dropTarget = new DropTarget(panel, DnDConstants.ACTION_COPY, this, true, null);
       
    }

    @Override
    public void drop(DropTargetDropEvent event) {
        try {
            
            DropTarget test = (DropTarget) event.getSource();
            
            Component ca = (Component) test.getComponent();
            Point dropPoint = ca.getMousePosition();
           
            System.out.println(p.getComponentAt(dropPoint));
            
            
            Transferable tr = event.getTransferable();

             if (event.isDataFlavorSupported(DataFlavor.imageFlavor)) {
                JLabel ico = (JLabel) tr.getTransferData(DataFlavor.imageFlavor);
                
             if (ico != null) {
                  
                
                    Slide slide=new Slide();
                    slide.setIcon(ico.getIcon());
                    slide.setName(ico.getName());
                      Iterator<Slide> timelineIterator = spokentutorials.SpokenTutorials.home.timelineSlides.iterator();
                      System.out.println(spokentutorials.SpokenTutorials.home.timelineSlides.size()+"yy");
            if(timelineIterator.hasNext()) {
                 if(p.getComponentAt(dropPoint).getClass()==JPanel.class)  {
                     System.out.println(dropPoint.getX()+":"+p.getComponents()[0].getBounds().getMaxX());
             if(dropPoint.getX()<p.getComponents()[0].getBounds().getMaxX()){
                 System.out.println("at start");
                 while(timelineIterator.hasNext()){
                     Slide current = timelineIterator.next();
                     current.setOrder(current.getOrder()+1);
                     
                 }
                 slide.setOrder(1);
             }
             else if(dropPoint.getX()>p.getComponents()[spokentutorials.SpokenTutorials.home.timelineSlides.size()-1].getBounds().getMaxX()){
                 System.out.println("at end");
                     slide.setOrder(spokentutorials.SpokenTutorials.home.timelineSlides.size()+1);
                  
             }
             else{
                 
                 Component[] pallete=p.getComponents();
                 for(int i=1;i<pallete.length;i++){
                    
                     if(dropPoint.getX()>pallete[i-1].getBounds().getMaxX()&&dropPoint.getX()<pallete[i].getBounds().getMinX()){
                         
                         slide.setOrder(i++);
                          System.out.println("between at "+i);
                         for(int j=i;j<pallete.length-1;j++){
                             Slide temp=spokentutorials.SpokenTutorials.home.timelineSlides.get(j);
                             temp.setOrder(i++);
                         }
                         break;
                         }
                     }
                 }
                 
            }

                if(p.getComponentAt(dropPoint).getClass()==JLabel.class) {
                      Component[] pallete=p.getComponents();
                      System.out.println("oreached"+pallete.length);
                 for(int i=0;i<pallete.length;i++){
                     System.out.println(dropPoint.getX()+"::"+pallete[i].getBounds().x);
                     if(pallete[i].getBounds().contains(dropPoint)){
                         
                         slide.setOrder(i++);
                          System.out.println("on  "+i);
                         for(int j=i;j<pallete.length-1;j++){
                             Slide temp=spokentutorials.SpokenTutorials.home.timelineSlides.get(j);
                             temp.setOrder(i++);
                         }
                         break;
                         }
                     }
                 }
            }
            else{
                slide.setOrder(1);
             System.out.println("first Slide");
            }

                    spokentutorials.SpokenTutorials.home.addSlideTimeline(slide);
                    
//                     JLabel label=new JLabel();
//                label.setName(slide.getName());
//                 label.setIcon(slide.getIcon());
//                p.add(label);
                Drag.TimelineReorder.refresh(p);
                    p.revalidate();
                    p.repaint();
                    
                    event.dropComplete(true);
                }
            } else {
                event.rejectDrop();
            }
        } catch (Exception e) {
            e.printStackTrace();
            event.rejectDrop();
        }
    }
}
