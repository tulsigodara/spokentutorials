package poi;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.apache.poi.xslf.usermodel.*;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.sl.usermodel.SlideShow;
import static spokentutorials.SpokenTutorials.workspace;
//import org.apache.commons.lang.*;

public class PPTToImages {
	public String location,filename;
        public ArrayList<String>files;
	public void getImages()
	{
		
	}


	public PPTToImages(String presentation,String location) throws Exception
	{
        String[] subDirs = new String[10];
        presentation=new File(presentation).getAbsolutePath();
         filename=new File(presentation).getName();
            
               
            
		System.out.println(location);
		
		
        
			File file=new File(presentation);
                        System.out.println(file);
		      XMLSlideShow ppt = new XMLSlideShow(new FileInputStream(file));
		      
		      //getting the dimensions and size of the slide 
		      Dimension pgsize = ppt.getPageSize();
		      List<XSLFSlide> slide = ppt.getSlides();
		      files=new ArrayList<String>();
		      for (int i = 0; i < slide.size(); i++) {
		         BufferedImage img = new BufferedImage(pgsize.width, pgsize.height,BufferedImage.TYPE_INT_RGB);
		         Graphics2D graphics = img.createGraphics();
	
		         //clear the drawing area
		         graphics.setPaint(Color.white);
		         graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width, pgsize.height));
	
		         //render
		         slide.get(i).draw(graphics);
		         
		       //creating an image file as output
		         //String projectPath=new File(path).getParentFile().getAbsolutePath();
		         String fileName=filename+(Integer.toString(i+1))+".jpg";
                        
		         fileName=new File(location,fileName).getAbsolutePath();
                         files.add(fileName);
		         FileOutputStream out = new FileOutputStream(fileName);
		         javax.imageio.ImageIO.write(img, "jpg", out);
		         ppt.write(out);
		         out.close();
		      }
				
                
	}
        public static void main(String[] args) {
            
            try {
                new PPTToImages("C:\\Users\\Joey\\Documents\\ppt\\Transforming Lucknow.pptx","C:\\Users\\Joey\\SpokenTutorials\\temp\\49");
            } catch (Exception ex) {
                Logger.getLogger(PPTToImages.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
}