/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spokentutorials;




import Dialogs.Wait;
import Xuggler.CaptureScreenToFile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javamedia.ScreenRecording;
import javax.media.CannotRealizeException;
import javax.media.IncompatibleSourceException;
import javax.media.NoDataSourceException;
import javax.media.NoPlayerException;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.Timer;
import static spokentutorials.SpokenTutorials.workspace;

/**
 *
 * @author Joey
 */
public class Recording {
    ArrayList<Slide>Record;
    boolean initialized=true;
    ScreenRecording recording;
    long startTime ;
    public static boolean record=false;
     public static boolean pause=false;
    public Recording(){
        Record=new ArrayList<Slide>();
       recording=new ScreenRecording();
    }
    public boolean startRecording(){
        new Dialogs.Countdown().start();
            new Thread(new Runnable() {
    public void run() { 
        record=true;
        try {
            Thread.sleep(3500);
            CaptureScreenToFile.startScreenCapture();
        } catch (LineUnavailableException ex) {
            ex.printStackTrace();
           killRecording();
          initialized=false;
        } catch (InterruptedException ex) {
            Logger.getLogger(Recording.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
}).start();
           startTime = System.currentTimeMillis();
            
       
    return initialized;   
    }
    public void stopRecording(){
        Recording.record=false;
        long endTime=System.currentTimeMillis();
        long elapsedMilliSeconds = endTime - startTime;
double elapsedSeconds = elapsedMilliSeconds / 1000.0;
    }
    public void killRecording(){
        Wait wait=new Wait();
                        wait.setMessage("Hardware unavilable", "Please close other applications using audio devices and restart application");
                        wait.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		
    }
}
