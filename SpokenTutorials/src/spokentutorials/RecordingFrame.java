/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spokentutorials;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Window;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author Joey
 */
public class RecordingFrame {
   static  Window w;
    public RecordingFrame() {
       w=new Window(null)
{ 
  @Override 
  public void paint(Graphics g)
  { 
    final Font font = getFont().deriveFont(48f);
    g.setFont(font);
    g.setColor(Color.RED);
    
    g.drawRect(0, 0, 800, 600);
  } 
  @Override 
  public void update(Graphics g)
  { 
    paint(g);
  } 
}; 
w.setAlwaysOnTop(true);
w.setBounds(w.getGraphicsConfiguration().getBounds());
w.setBackground(new Color(0, true));
  }
    public void showFrame(){
        w.setVisible(true); 
    }
    public void hideFrame(){
        w.setVisible(false); 
    }
}
