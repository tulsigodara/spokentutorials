/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Xuggler;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Joey
 */
public class VideoImage extends JPanel { 
 
    Image image;
 
    public void setImage(final Image image) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() { 
                VideoImage.this.image = image;
                repaint(); 
            } 
        }); 
    } 
 
    @Override 
    public synchronized void paint(Graphics g) {
        if (image != null) {
            g.drawImage(image, 0, 0, null);
        } 
    } 
}

