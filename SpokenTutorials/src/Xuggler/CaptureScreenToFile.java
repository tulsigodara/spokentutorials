package Xuggler;

import Dialogs.Wait;
import com.xuggle.ferry.IBuffer;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;

import com.xuggle.mediatool.ToolFactory;
import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.xuggler.IAudioSamples;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IRational;
import java.awt.AWTException;
import java.io.File;
import static java.lang.Math.random;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import spokentutorials.Recording;
import static spokentutorials.SpokenTutorials.*;

/**
 * Using {@link IMediaWriter}, takes snapshots of your desktop and
 * encodes them to video.
 * 
 * @author aclarke
 * 
 */

public class CaptureScreenToFile
{
  private static IRational FRAME_RATE=IRational.make(3,1);
  private static final int SECONDS_TO_RUN_FOR = 15;
 
  /**
   * Takes a screen shot of your entire screen and writes it to
   * output.flv
   * 
   * @param args
   */
  public static void startScreenCapture() throws LineUnavailableException 
  {
    try
    {
      final String outFile;
       TargetDataLine line = null; 
       Date date=new Date();
       

SimpleDateFormat sdf=new SimpleDateFormat("ddMMyy-hhmmss");
   
        outFile =  spokentutorials.SpokenTutorials.workspace+"\\"+sdf.format( new Date() )+".mp4";
        System.out.println("saving to "+ outFile);
        spokentutorials.Presenter.outputFile=outFile;
      // This is the robot for taking a snapshot of the
      // screen.  It's part of Java AWT
      final Robot robot = new Robot();
      final Toolkit toolkit = Toolkit.getDefaultToolkit();
      final Rectangle screenBounds = new Rectangle();
      screenBounds.setBounds(0, 0, 800, 600);
      // First, let's make a IMediaWriter to write the file.
      final IMediaWriter writer = ToolFactory.makeWriter(outFile);
      
      // We tell it we're going to add one video stream, with id 0,
      // at position 0, and that it will have a fixed frame rate of
      // FRAME_RATE.
      writer.addVideoStream(0, 0,
          FRAME_RATE,
          800, 600);
      
      // Now, we're going to loop
      long startTime = System.nanoTime();
      
      
      
      
      
      
      
      AudioFormat audioFormat=getAudioFormat();
      
      
		int bufferSize = (int) audioFormat.getSampleRate()
				* audioFormat.getFrameSize();
		byte audioBuf[] = new byte[bufferSize];

		final int audioStreamIndex = 1;
			final int audioStreamId = 1;
			final int channelCount = 1;
			
			final int sampleRate = (int) audioFormat.getSampleRate(); // Hz
			int audionumber = writer.addAudioStream(audioStreamIndex,
					audioStreamId, channelCount, sampleRate);

			
				
      DataLine.Info info = new DataLine.Info(TargetDataLine.class,
				audioFormat);
		
          line = (TargetDataLine) AudioSystem.getLine(info);
			line.open(audioFormat);
			line.start();
			// System.out.println("line opened: " + line.);
			// audioInputStream = new AudioInputStream(line);

		
      
      
      
      
      
      
      
      
      
      while (spokentutorials.Recording.record)
      {
          while(spokentutorials.Recording.pause ){System.out.println("paused");continue;}
        // take the screen shot
        BufferedImage screen;
        if(!presenter.isVisible()){screen = robot.createScreenCapture(screenBounds);System.out.println(screen.getHeight()+":"+screen.getWidth());}
        else{
                      
            screen=presenter.currentSlide.getBufferedImage();
            System.out.println(screen.getHeight()+":"+screen.getWidth());
        }
        // convert to the right image type
        BufferedImage bgrScreen = convertToType(screen,
            BufferedImage.TYPE_3BYTE_BGR);
        
        // encode the image
        writer.encodeVideo(0,bgrScreen,
            System.nanoTime()-startTime, TimeUnit.NANOSECONDS);

        
        //encode Audio
        
        long currenttime = System.currentTimeMillis();
	            long difft = currenttime - startTime;
	            long diff = (long) (difft / 1000 % 60);
	            if (10 < diff) {
	            	line.stop();
                        line.close();
	    			writer.close();

	            } 
	            else{
			
				long nanoTs = System.nanoTime() - startTime;

				// encode the image to stream #0

	

				// get and encode audio
				int nBytesRead = line.read(audioBuf, 0, line.available());
				if (nBytesRead > 0) {
					System.out.println("Read bytes: " + nBytesRead);
					// encode audio to stream #1
					IBuffer iBuf = IBuffer.make(null, audioBuf, 0, nBytesRead);

					IAudioSamples smp = IAudioSamples.make(iBuf, 1,
							IAudioSamples.Format.FMT_S16);
					// IAudioSamples smp = IAudioSamples.make(nBytesRead, 1);
					// smp.put(audioBuf, 0, 0, nBytesRead);
					if (smp == null)
						continue;
					long numSample = nBytesRead / smp.getSampleSize();// smp.getNumSamples();//*/
																		// 44100;//should
																		// be
																		// tthe
																		// number
																		// of
																		// samples
																		// microphone
					System.out.println("NUM SAMPLE " + numSample + " SMP size"
							+ smp.getSampleSize());
					smp.setComplete(true, numSample,
							(int) audioFormat.getSampleRate(),
							audioFormat.getChannels(),
							IAudioSamples.Format.FMT_S16, nanoTs / 1000);
					System.out.println("encode SAMPLES ");

					writer.encodeAudio(audionumber, smp);
        
                                }
                    }
        
        
        
        
        
        
        // sleep for framerate milliseconds
        Thread.sleep((long) (1000 / FRAME_RATE.getDouble()));

      }
      // Finally we tell the writer to close and write the trailer if
      // needed
      if(line.isOpen())line.close();
      writer.close();
    }
  
  /**
   * Convert a {@link BufferedImage} of any type, to {@link BufferedImage} of a
   * specified type. If the source image is the same type as the target type,
   * then original image is returned, otherwise new image of the correct type is
   * created and the content of the source image is copied into the new image.
   * 
   * @param sourceImage
   *          the image to be converted
   * @param targetType
   *          the desired BufferedImage type
   * 
   * @return a BufferedImage of the specifed target type.
   * 
   * @see BufferedImage
   */

      catch (InterruptedException ex) {
          Logger.getLogger(CaptureScreenToFile.class.getName()).log(Level.SEVERE, null, ex);  /**
   * Convert a {@link BufferedImage} of any type, to {@link BufferedImage} of a
   * specified type. If the source image is the same type as the target type,
   * then original image is returned, otherwise new image of the correct type is
   * created and the content of the source image is copied into the new image.
   * 
   * @param sourceImage
   *          the image to be converted
   * @param targetType
   *          the desired BufferedImage type
   * 
   * @return a BufferedImage of the specifed target type.
   * 
   * @see BufferedImage
   */
      } catch (AWTException ex) {
          Logger.getLogger(CaptureScreenToFile.class.getName()).log(Level.SEVERE, null, ex);
      }
      }
  public static BufferedImage convertToType(BufferedImage sourceImage,
      int targetType)
  {
    BufferedImage image;

    // if the source image is already the target type, return the source image

    if (sourceImage.getType() == targetType)
      image = sourceImage;

    // otherwise create a new image of the target type and draw the new
    // image

    else
    {
      image = new BufferedImage(sourceImage.getWidth(),
          sourceImage.getHeight(), targetType);
      image.getGraphics().drawImage(sourceImage, 0, 0, null);
    }

    return image;
  }

    

        private static AudioFormat getAudioFormat() {
		float sampleRate = 8000.0F;
		// 8000,11025,16000,22050,44100
		int sampleSizeInBits = 16;
		// 8,16
		int channels = 1;
		// 1,2
		boolean signed = true;
		// true,false
		boolean bigEndian = false;
		// true,false
		return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed,
				bigEndian);
	}
    

}